#!/usr/bin/python3

import sys
from PyQt4 import QtCore, QtGui
import requests
import urllib

# Create an PyQT4 application object.
a = QtGui.QApplication(sys.argv)
 
# The QWidget widget is the base class of all user interface objects in PyQt4.
w = QtGui.QWidget()

textbox = QtGui.QLineEdit(w)
textbox.move(10, 10)
textbox.resize(740,40)

pageMonitor = QtGui.QLabel(w)
pageMonitor.move(10,60)
pageMonitor.resize(700,40)
pageMonitor.setText("")
pageMonitor.setAlignment(QtCore.Qt.AlignTop)
pageMonitor.setWordWrap(True)

webview = QtGui.QLabel(w)
webview.move(10,100)
webview.resize(700,440)
webview.setText("Type a url!")
webview.setAlignment(QtCore.Qt.AlignTop)
webview.setTextFormat(QtCore.Qt.RichText)
webview.setWordWrap(True)

def linkClicked(url):
	textbox.setText(url)
	go()

webview.linkActivated.connect(linkClicked)

pages = [""]
currentPage = 0

def scrollUp():
	global currentPage
	global pages
	currentPage -= 1
	if(currentPage >= 0):
		webview.setText(pages[currentPage])
	else:
		currentPage += 1

	pageMonitor.setText("Page "+str(currentPage+1)+" / "+str(len(pages)))

def scrollDown():
	global currentPage
	global pages
	currentPage += 1
	if(currentPage <= len(pages)):
		webview.setText(pages[currentPage])
	else:
		currentPage -= 1

	pageMonitor.setText("Page "+str(currentPage+1)+" / "+str(len(pages)))

def go():

	if(textbox.text() == ""):
		return
	global pages
	pages = [""]
	url = textbox.text()
	r = requests.get("http://192.168.1.93:8081/page/"+url)
	# Parse Markdown
	md = r.text

	md = md.splitlines()

	pageNum = 0

	j = 0

	for line in md:
		tmp = line.split()
		i = 0
		j = j + 1
		if(j % 10 == 0):
			pageNum = pageNum + 1
			pages.append("")
	
		currentTag = ""
		isLink = False
		for word in tmp:
			if(i == 0):
				if(word == "#"):
					pages[pageNum] += "<h1>"
					currentTag = "h1"
				elif(word == "##"):
					pages[pageNum] += "<h2>"
					currentTag = "h2"
				elif(word == "###"):
					pages[pageNum] += "<h3>"
					currentTag = "h3"
				elif(word == "####"):
					pages[pageNum] += "<h4>"
					currentTag = "h4"
				elif(word == "#####"):
					pages[pageNum] += "<h5>"
					currentTag = "h5"
				elif(word == "######"):
					pages[pageNum] += "<h6>"
					currentTag = "h6"
				elif(word == "__"):
					pages[pageNum] += "<stong>"
					currentTag = "strong"
				elif(word == "_"):
					pages[pageNum] += "<em>"
					currentTag = "em"
				elif(word == "@"):
					pages[pageNum] += "<a href='"
					currentTag ="a"
					isLink = True
				else:
					pages[pageNum] += "<p>" + word
					currentTag = "p"
			elif(isLink):
					pages[pageNum] += word + "'>" + word
					isLink = False
			else:
				pages[pageNum] += " " + word
			i = i + 1
		pages[pageNum] += "</"+currentTag+">"

	webview.setText(pages[0])
	pageMonitor.setText("Page "+str(currentPage+1)+" / "+str(len(pages)))

button = QtGui.QPushButton('Go', w)
button.move(750,10)
button.resize(40,40)
button.clicked.connect(go)

textbox.returnPressed.connect(button.click)

scrollBtnUp = QtGui.QPushButton('^', w)
scrollBtnUp.move(750,60)
scrollBtnUp.resize(40,40)
scrollBtnUp.clicked.connect(scrollUp)

scrollBtnDown = QtGui.QPushButton('v', w)
scrollBtnDown.move(750,100)
scrollBtnDown.resize(40,40)
scrollBtnDown.clicked.connect(scrollDown)

# Set window size.
w.resize(800,500)
 
# Set window title
w.setWindowTitle("Markdown Browser")
 
# Show window
w.show()
 
sys.exit(a.exec_())
